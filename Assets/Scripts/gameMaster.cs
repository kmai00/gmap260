﻿using UnityEngine;
using System.Collections;

public class gameMaster : MonoBehaviour {

	private float guiWidth = Screen.width/2, guiHeight = Screen.height/2;
	GameObject player1, player2, cubeSpawner;
	public GameObject cube_Player2, cube_Player1;
	float countDown = 5;
	bool countDownMode = true, gameOver = false, player1_wins = false, draw = false;

	// Use this for initialization
	void Start () {
		player1 = GameObject.Find("player1_Char");
		player2 = GameObject.Find("player2_Char");
		cubeSpawner = GameObject.Find("cubeSpawner");

		player1.GetComponent<Player_Controls>().enabled = false;
		player2.GetComponent<Player_Controls>().enabled = false;
		cubeSpawner.GetComponent<cubeSpawner>().enabled = false;
	}

	void OnGUI(){
		if(countDown > 0 ){
			Rect groupSize = new Rect(guiWidth - guiWidth/2, guiHeight - guiHeight/2, guiWidth,guiHeight/2);
			GUI.BeginGroup(groupSize);			
			var myStyle = GUI.skin.GetStyle("TextArea"); //find alternative style to make title stick out
			myStyle.fontSize = 40;
			myStyle.alignment = TextAnchor.UpperCenter;
			GUI.Label(new Rect(0,0,guiWidth,guiHeight), ((int) countDown).ToString() , myStyle);
		
			GUI.EndGroup();
		}
		if(gameOver){
			if(player1_wins){
				Rect groupSize = new Rect(guiWidth - guiWidth/2, guiHeight - guiHeight/2, guiWidth,guiHeight/2);
				GUI.BeginGroup(groupSize);			
				var myStyle = GUI.skin.GetStyle("TextArea"); //find alternative style to make title stick out
				myStyle.fontSize = 20;
				myStyle.alignment = TextAnchor.UpperCenter;
				GUI.Label(new Rect(0,0,guiWidth,guiHeight), "Player 1 wins!", myStyle);


				GUI.contentColor = Color.white;
				myStyle = GUI.skin.GetStyle("Button");
				myStyle.stretchWidth = true;
				myStyle.stretchHeight = false;
				if(GUI.Button(new Rect(groupSize.x/2, groupSize.y/4 + 15, guiWidth/2, guiHeight/8), "Replay", myStyle)){
					cube_Player1.rigidbody2D.gravityScale = 1;
					cube_Player2.rigidbody2D.gravityScale = 1;
					Application.LoadLevel("Scene1");
				}

				GUI.EndGroup();
			}
			else if(!player1_wins && !draw){
				Rect groupSize = new Rect(guiWidth - guiWidth/2, guiHeight - guiHeight/2, guiWidth,guiHeight/2);
				GUI.BeginGroup(groupSize);			
				var myStyle = GUI.skin.GetStyle("TextArea"); //find alternative style to make title stick out
				myStyle.fontSize = 20;
				myStyle.alignment = TextAnchor.UpperCenter;
				GUI.Label(new Rect(0,0,guiWidth,guiHeight), "Player 2 wins!", myStyle);


				GUI.contentColor = Color.white;
				myStyle = GUI.skin.GetStyle("Button");
				myStyle.stretchWidth = true;
				myStyle.stretchHeight = false;
				if(GUI.Button(new Rect(groupSize.x/2, groupSize.y/4 + 15, guiWidth/2, guiHeight/8), "Replay", myStyle)){
					cube_Player1.rigidbody2D.gravityScale = 1;
					cube_Player2.rigidbody2D.gravityScale = 1;
					Application.LoadLevel("Scene1");
				}

				GUI.EndGroup();
			}
			else if (draw){
				Rect groupSize = new Rect(guiWidth - guiWidth/2, guiHeight - guiHeight/2, guiWidth,guiHeight/2);
				GUI.BeginGroup(groupSize);			
				var myStyle = GUI.skin.GetStyle("TextArea"); //find alternative style to make title stick out
				myStyle.fontSize = 20;
				myStyle.alignment = TextAnchor.UpperCenter;
				GUI.Label(new Rect(0,0,guiWidth,guiHeight), "Draw!", myStyle);
				
				
				GUI.contentColor = Color.white;
				myStyle = GUI.skin.GetStyle("Button");
				myStyle.stretchWidth = true;
				myStyle.stretchHeight = false;
				if(GUI.Button(new Rect(groupSize.x/2, groupSize.y/4 + 15, guiWidth/2, guiHeight/8), "Replay", myStyle)){
					cube_Player1.rigidbody2D.gravityScale = 1;
					cube_Player2.rigidbody2D.gravityScale = 1;
					Application.LoadLevel("Scene1");
				}
				GUI.EndGroup();
			}
		}
	}

	void Update () {
		countDown -= Time.deltaTime;
		if(countDown <= 0 && countDownMode){
			player1.GetComponent<Player_Controls>().enabled = true;
			player2.GetComponent<Player_Controls>().enabled = true;
			cubeSpawner.GetComponent<cubeSpawner>().enabled = true;
			countDownMode = false;
		}

		bool player1_alive = player1.GetComponent<Player_Controls>().getAlive();
		bool player2_alive = player2.GetComponent<Player_Controls>().getAlive();

		if(!player1_alive && !player2_alive && !gameOver){
			cubeSpawner.GetComponent<cubeSpawner>().enabled = false;
			draw = true;
			gameOver = true;
		}
		else if(!player1_alive && !gameOver){
			gameOver = true;
			cubeSpawner.GetComponent<cubeSpawner>().enabled = false;
			player1_wins = false;
		}
		else if(!player2_alive && !gameOver){
			cubeSpawner.GetComponent<cubeSpawner>().enabled = false;
			player1_wins = true;
			gameOver = true;
		}
	}
}
