﻿using UnityEngine;
using System.Collections;

public abstract class Player_Controls : MonoBehaviour {
	
	private int magnitude = 15;
	private bool isGround = false, isFacingRight = true;
	private int mask;
	
	private float messageTimer;
	private  bool powerActive = false;
	private string powerName = "";
	private Color color;
	
	public Transform lineStart, lineEnd;
	
	private AudioSource jumpSFX, crushSFX, powerSFX;
	
	protected Animator anim;
	protected string player;
	protected float screenWidth = Screen.width;

	protected bool slowDownOn = false;
	protected float slowDownTimer;
	
	protected bool isAlive = true;

	protected float special_X, special_Y;

	// Use this for initialization
	 protected void Start () {
		AudioSource[] sfxList = GetComponents<AudioSource>();
		jumpSFX = sfxList[0];
		crushSFX = sfxList[1];
		powerSFX = sfxList[2];
		
		mask = 1 << LayerMask.NameToLayer("ground");
		anim = GetComponent<Animator>();
	}
	
	void OnGUI(){
		var myStyle = GUI.skin.GetStyle("TextArea"); //find alternative style to make title stick out
		myStyle.fontSize = 15;
		myStyle.alignment = TextAnchor.UpperCenter;
		GUI.Label(new Rect(Camera.main.WorldToScreenPoint(this.transform.position).x - 45, Screen.height - Camera.main.WorldToScreenPoint(this.transform.position).y + 
		                   35,100,25), player, myStyle);

		if(powerActive){
			myStyle = GUI.skin.GetStyle("TextArea"); //find alternative style to make title stick out
			myStyle.fontSize = 15;
			myStyle.alignment = TextAnchor.MiddleCenter;
			GUI.color  = color;
			GUI.Label(new Rect(special_X,special_Y,100,25), powerName, myStyle);
			color.a -= Time.deltaTime/2;
		}
	}
	
	// Update is called once per frame
	void Update () {

		messageTimer -= Time.deltaTime;
		if(messageTimer < 0)
			powerActive = false;

		if(slowDownOn){
			slowDownTimer -= Time.deltaTime;
			if(slowDownTimer <= 0){
				slowDownOn = false;
				setMag(15);
			}

		}

		lineStart.transform.position = this.transform.position + new Vector3(0, (float)-0.55, 0);
		
		float horizontal = Input.GetAxis( player + "_Horizontal");
		if(horizontal < 0){
			if(isFacingRight){
				transform.localScale  =  new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
				isFacingRight = false;
			}
		}
		else if(horizontal > 0){
			if(!isFacingRight){
				transform.localScale  =  new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
				isFacingRight = true;		
			}
		}
		Vector2 velocity = rigidbody2D.velocity;
		velocity.x = horizontal * magnitude;
		rigidbody2D.velocity = velocity;
		
		anim.SetFloat("speed", Mathf.Abs(velocity.x));
		
		if(Input.GetButton( player + "_Jump") && isGround){
			rigidbody2D.AddForce(Vector2.up * 100);
			isGround = false;
			jumpSFX.Play();
			anim.SetBool("jump", !isGround);
			//Debug.Log("Jump");
		}
		
		Debug.DrawLine(lineStart.transform.position, lineEnd.transform.position);
		
		if(Physics2D.Linecast(lineStart.transform.position, lineEnd.transform.position,mask)){
			isGround = true;
			anim.SetBool("jump", !isGround);
		}
	}
	
	void OnCollisionEnter2D(Collision2D collision){
		if(collision.gameObject.name == "Cube_" + player + "(Clone)"|| collision.gameObject.name == "plane(Clone)"){
			crushSFX.Play();
			anim.SetBool("dead", true);
			GetComponent<Player_Controls>().enabled = false;
			isAlive = false;
		}
		if(collision.gameObject.name == "powerCube(Clone)"){
			powerSFX.Play();
			color = Color.white;
			powerUp();
			special_X = Camera.main.WorldToScreenPoint(collision.transform.position).x;
			special_Y = Camera.main.WorldToScreenPoint(collision.transform.position).y;
			DestroyObject(collision.gameObject);
		}
	}

	void powerUp(){
		int i = Random.Range(0,3);
		//Debug.Log(player + "Special: "  + i);
		switch(i){
		case 0:
			powerName = "Fast Drop!";
			messageTimer = 5;
			increaseSpeed();
			break;
		case 1:
			powerName = "Slow Down!";
			messageTimer = 5;
			slowDown();
			break;
		case 2:
			powerName = "PLANE!";
			messageTimer = 3;
			dropPlane();
			break;
		}
		powerActive = true;
	}
		
	public void setMag(int magIn){
		magnitude = magIn;
		slowDownOn = true;
		slowDownTimer = 5;
	}

	public bool getAlive(){
		return isAlive;
	}

	public void dropPlane(){
		GameObject.Find("cubeSpawner").GetComponent<cubeSpawner>().DropPlane(player);
	}

	public abstract void slowDown();

	public abstract void increaseSpeed();
}
