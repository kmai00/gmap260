﻿using UnityEngine;
using System.Collections;

public class Title : MonoBehaviour {

	private float guiWidth = Screen.width/2, guiHeight = Screen.height/2;
	bool mainMenu = true;
	public GUISkin skySkin;
	public Texture skyBlock, powerUp;

	void OnGUI(){
		GUI.skin = skySkin;
		GUI.Label(new Rect(Screen.width-210, Screen.height-60, 300, 100), "Kevin Mai\nShihong Zhong\nKevin P Hor");

		if(mainMenu){		

			GUI.contentColor = Color.black;
			var myStyle = GUI.skin.GetStyle("Button");
			myStyle.stretchWidth = true;
			myStyle.stretchHeight = false;
			myStyle.fontSize = 30;

			if(GUI.Button(new Rect(guiWidth/4, guiHeight + 150, guiWidth/2, guiHeight/5), "Play", myStyle)){
				Application.LoadLevel("Scene1");
			}
			myStyle.fontSize = 25;
			if(GUI.Button(new Rect(guiWidth + guiWidth/4 , guiHeight + 150, guiWidth/2, guiHeight/5), "How to Play", myStyle)){
				mainMenu = false;

			}

		}
		else{
			var myStyle = GUI.skin.GetStyle("TextArea");
			myStyle.fontSize = 20;
			myStyle.alignment = TextAnchor.UpperCenter;
			GUI.Label(new Rect(guiWidth/2,guiHeight/2,guiWidth,guiHeight), "\n\nAvoid the falling sky blocks" +
			          "\n\n\n\nPower up to thwart your opponent" +
			          "\n\n\nPlayer 1: WASD" +
			          "\n\n\nPlayer 2: Arrow keys", myStyle);
			myStyle = GUI.skin.GetStyle("Button");
			myStyle.stretchWidth = true;
			myStyle.stretchHeight = false;
			GUI.contentColor = Color.black;
			GUI.DrawTexture(new Rect(guiWidth + 340,guiHeight/2 + 20,50,50), skyBlock, ScaleMode.ScaleToFit);
			GUI.DrawTexture(new Rect(guiWidth + 340,guiHeight/2 + 100,50,50), powerUp, ScaleMode.ScaleToFit);
			if(GUI.Button(new Rect(guiWidth, guiHeight + guiHeight/2, guiWidth/2, guiHeight/8), "Back", myStyle)){
				mainMenu = true;
			}
		}
	}
}
