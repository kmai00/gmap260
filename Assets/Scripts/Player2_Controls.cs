﻿using UnityEngine;

public class Player2_Controls : Player_Controls {
	void Start () {
		base.Start();
		player = "Player2";
		screenWidth -= 100;
	}

	public override void slowDown ()
	{
		GameObject.Find("player1_Char").GetComponent<Player_Controls>().setMag(8);
	}

	public override void increaseSpeed(){
		GameObject.Find("cubeSpawner").GetComponent<cubeSpawner>().IncreaseDropPlayer1();
	}
}
