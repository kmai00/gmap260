﻿using UnityEngine;
using System.Collections;

public class cubeSpawner : MonoBehaviour {
	public GameObject cube_Player2, cube_Player1, powerCube,plane;
	private float cubeSpawn = (float) 0.5;
	private float specialCubespawn = (float) 8;
	public float height = 23;
	public float player1_wall_max = -15, player1_wall_min = (float) -1.5;

	private float gravityScale = 5;
	private bool specialEffect_Player1 = false, specialEffect_Player2 = false;
	private float specialEffectTimer_Player1, specialEffectTimer_Player2;
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		
		Vector3 player2_cubeVector = new Vector3(Random.Range(player1_wall_max,player1_wall_min), height,0);
		Vector3 player1_cubeVector = new Vector3(Random.Range(-1 *player1_wall_max, -1* player1_wall_min), height,0);
		//Vector3 player1_cubeVector = player2_cubeVector;
		//player1_cubeVector.x = player1_cubeVector.x * -1;
		//Debug.Log(player1_cubeVector.x + "----" + player2_cubeVector.x);
		
		cubeSpawn -= Time.deltaTime;

		if(cubeSpawn <= 0){
			Instantiate(cube_Player2, player1_cubeVector, Quaternion.identity);
			Instantiate(cube_Player1, player2_cubeVector, Quaternion.identity);

			cubeSpawn = (float) 0.5;
		}

		player1_cubeVector = new Vector3(Random.Range(player1_wall_max,player1_wall_min), height,0);
		player2_cubeVector = new Vector3(Random.Range(player1_wall_max,player1_wall_min), height,0);
		//player2_cubeVector = player1_cubeVector;
		player2_cubeVector.x = player2_cubeVector.x * -1;

		specialCubespawn -= Time.deltaTime;
		if(specialCubespawn <= 0){
			Instantiate(powerCube, player1_cubeVector, Quaternion.identity);
			Instantiate(powerCube, player2_cubeVector, Quaternion.identity);
			specialCubespawn = (float) 8;
		}

		IncreaseDropTimer();

	}

	public void DropPlane(string player){
		Vector3 player1_cubeVector = new Vector3(Random.Range(player1_wall_max,player1_wall_min), height,0);
		Vector3 player2_cubeVector = player1_cubeVector;
		player2_cubeVector.x = player2_cubeVector.x * -1;

		if(!player.Equals("Player1"))
			Instantiate(plane, player1_cubeVector, Quaternion.identity);
		else
			Instantiate(plane, player2_cubeVector, Quaternion.identity);
	}

	#region DropSpeed

	private void IncreaseDropTimer(){
		if(specialEffect_Player1){
			specialEffectTimer_Player1 -= Time.deltaTime;
			if(specialEffectTimer_Player1 <= 0){
				specialEffect_Player1 = false;
				cube_Player1.rigidbody2D.gravityScale = 1;
			}
		}

		if(specialEffect_Player2){
			specialEffectTimer_Player2 -= Time.deltaTime;
			if(specialEffectTimer_Player2 <= 0){
				specialEffect_Player2 = false;
				cube_Player2.rigidbody2D.gravityScale =1;
			}
		}
	}

	public void IncreaseDropPlayer2(){
		cube_Player2.rigidbody2D.gravityScale = gravityScale;
		specialEffect_Player2 = true;
		specialEffectTimer_Player2 = (float) 5;
	}

	public void IncreaseDropPlayer1(){
		cube_Player1.rigidbody2D.gravityScale = gravityScale;
		specialEffect_Player1 = true;
		specialEffectTimer_Player1 = (float) 5;
	}
	#endregion
}
