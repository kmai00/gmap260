﻿using UnityEngine;

public class Player1_Controls : Player_Controls {
	void Start () {
		base.Start();
		player = "Player1";
		screenWidth = 0;
	}

	public override void slowDown ()
	{
		GameObject.Find("player2_Char").GetComponent<Player_Controls>().setMag(8);
	}

	public override void increaseSpeed(){
		GameObject.Find("cubeSpawner").GetComponent<cubeSpawner>().IncreaseDropPlayer2();
	}

}
